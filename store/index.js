export const state = () => ({
  orderStatus: [
    { value: 1, text: 'Πρόχειρη' },
    { value: 2, text: 'Νέα' },
    { value: 3, text: 'Εγκεκριμένη' },
    { value: 4, text: 'Εκτελείται' },
    { value: 5, text: 'Ολοκληρωμένη' }
  ]
})
