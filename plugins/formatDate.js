import Vue from 'vue'
import { DateTime } from 'luxon'

export default () => {
  Vue.filter('formatDate', function (value, format = 'dd/MM/yyyy') {
    if (!value) return ''
    return DateTime.fromISO(value).setLocale('el').toFormat(format)
  })
}
