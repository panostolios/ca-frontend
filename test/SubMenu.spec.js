// SubMenu component accepts a title an icon and an array of MenuItems
// and renders a Vuetify <v-group-list>
import Vuetify from 'vuetify'

import { shallowMount, createLocalVue } from '@vue/test-utils'
import SubMenu from '@/components/navigation/SubMenu'

const localVue = createLocalVue()

describe('SubMenu', () => {
  const vuetify = new Vuetify()

  const propsData = {
    title: 'Order List',
    icon: 'mdi-playlist_add_check',
    items: [
      {
        title: 'All Orders',
        icon: 'mdi-apps',
        to: '/orders'
      },
      {
        title: 'Active Orders',
        icon: 'mdi-apps',
        to: '/orders?status=active'
      },
      {
        title: 'Approved Orders',
        icon: 'mdi-apps',
        to: '/orders?status=approved'
      },
      {
        title: 'Orders in progress',
        icon: 'mdi-apps',
        to: '/orders?status=in_progress'
      }
    ]
  }

  const wrapper = shallowMount(SubMenu, {
    localVue,
    vuetify,
    propsData
  })

  test('is a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test('accepts the right props', () => {
    for (const prop in wrapper.props()) {
      expect(wrapper.props(prop)).toBe(propsData[prop])
    }
  })

  test('renders all sub-items', () => {
    const items = wrapper.findAll('v-list-item')
    expect(items.length).toBe(4)
  })

  test('renders correct sub-items title', () => {
    const vListItemTitles = wrapper.findAll('v-list-item-title').wrappers
    for (const [i, title] of vListItemTitles.entries()) {
      expect(title.text()).toBe(propsData.items[i].title)
    }
  })

  test('each item redirects to the correct URL', () => {
    const vListItems = wrapper.findAll('v-list-item').wrappers
    for (const [i, listItem] of vListItems.entries()) {
      expect(listItem.attributes().to).toBe(propsData.items[i].to)
    }
  })
})
