import Vuetify from 'vuetify'

import { mount, createLocalVue } from '@vue/test-utils'
import MenuCategory from '@/components/navigation/MenuCategory'

const localVue = createLocalVue()

describe('MenuCategory', () => {
  const vuetify = new Vuetify()

  const propsData = {
    name: 'Orders',
    items: [
      {
        title: 'New Order',
        icon: 'mdi-apps',
        to: '/orders/new'
      },
      {
        title: 'Order List',
        icon: 'mdi-playlist_add_check',
        items: [
          {
            title: 'All Orders',
            icon: 'mdi-apps',
            to: '/orders'
          },
          {
            title: 'Active Orders',
            icon: 'mdi-apps',
            to: '/orders?status=active'
          },
          {
            title: 'Approved Orders',
            icon: 'mdi-apps',
            to: '/orders?status=approved'
          },
          {
            title: 'Orders in progress',
            icon: 'mdi-apps',
            to: '/orders?status=in_progress'
          }
        ]
      }
    ]
  }

  const wrapper = mount(MenuCategory, {
    localVue,
    vuetify,
    propsData
  })

  test('is a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test('accepts the right props', () => {
    const expectedProps = Object.keys(propsData)
    expect(expectedProps).toContain('name')
    expect(expectedProps).toContain('items')
  })

  test('renders the correct items', () => {
    const items = wrapper.findAll('[type = "menu-item"]').wrappers
    const subMenus = wrapper.findAll('[type = "sub-menu"]').wrappers
    expect(subMenus).toHaveLength(1)
    expect(items).toHaveLength(1)
  })
})
