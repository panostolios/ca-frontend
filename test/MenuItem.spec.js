// MenuItem component accepts a title an icon and a url
// and renders a Vuetify <v-list-item>

import Vuetify from 'vuetify'

import { shallowMount, createLocalVue } from '@vue/test-utils'
import MenuItem from '@/components/navigation/MenuItem'

const localVue = createLocalVue()

describe('MenuItem', () => {
  const vuetify = new Vuetify()

  const propsData = {
    title: 'New Customer',
    icon: 'mdi-account',
    to: '/customers/new'
  }

  const wrapper = shallowMount(MenuItem, {
    localVue,
    vuetify,
    propsData
  })

  test('is a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test('accepts the right props', () => {
    for (const prop in wrapper.props()) {
      expect(wrapper.props[prop]).toBe(propsData.prop)
    }
  })

  test('redirects to the correct url', () => {
    expect(wrapper.attributes().to).toBe(propsData.to)
  })

  test('renders the icon', () => {
    const vListIcon = wrapper.find('v-list-item-icon')
    expect(vListIcon.text()).toBe(propsData.icon)
  })

  test('renders the title', () => {
    const vListItemContent = wrapper.find('v-list-item-content')
    expect(vListItemContent.text()).toBe(propsData.title)
  })
})
